from settings.configs.base import *

DEBUG = False


DOMAIN = variables.get("DOMAIN")
DOMAINS = variables.get("DJANGO_ALLOWED_HOSTS").split(' ')
SESSION_COOKIE_DOMAIN = DOMAIN
CSRF_COOKIE_DOMAIN = DOMAIN
ALLOWED_HOSTS = DOMAINS
SITE_ID = 2

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': variables.get("SQL_DATABASE"),
        'USER': variables.get("SQL_USER"),
        'PASSWORD': variables.get("SQL_PASSWORD"),
        'HOST': variables.get("SQL_HOST"),
    }
})
