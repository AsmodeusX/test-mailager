# from settings.configs.base import *
from .base import *

DEBUG = True

DOMAIN = variables.get("DOMAIN")
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

EMAIL_PORT = 587
EMAIL_USE_SSL = False
EMAIL_USE_TLS = True
SECURE_SSL_REDIRECT = False
ALLOWED_HOSTS = (
    DOMAIN,
    'localhost'
)
DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': variables.get("DATABASE_NAME"),
        'USER': variables.get("DATABASE_USER"),
        'PASSWORD': variables.get("DATABASE_PASSWORD"),
        'HOST': variables.get("DATABASE_HOST"),
        'PORT': variables.get("DATABASE_PORT"),
    }
})
