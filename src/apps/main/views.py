from django.template import loader
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView
from django.template import RequestContext
from .models import MainPageConfig
from newsletters.models import Newsletter


class MainPageView(TemplateView):
    template_name = 'main/index.html'

    def get(self, request, *args, **kwargs):
        config = MainPageConfig.get_solo()
        newslettters = Newsletter.objects.all().order_by('-sended')

        return self.render_to_response({
            'page': config,
            'newsletters': newslettters,
        },  )
