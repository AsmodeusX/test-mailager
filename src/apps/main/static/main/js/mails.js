(function($) {
    $(document).ready(function() {
        var csrfmiddlewaretoken = $('#newsletters').find('[name="csrfmiddlewaretoken"]').val();
        var $btnSend  = $(".btn-send");
        $btnSend.on('click', function() {
            var $btn = $(this);
            var newsletterId = $btn.data('newsletter');
            $.ajax({
                url: "/newsletters/send/",
                type: 'GET',
                dataType: 'json',
                data:{
                    'newsletter_id': newsletterId,
                    'csrfmiddlewaretoken': csrfmiddlewaretoken
                },
                
                success: function (response) {
                    $btn.addClass("disabled");
    
                },
            });
        })
    })
})(jQuery);