(function($) {
    $("#addNewsletter").on("click", function() {
        $.ajax({
            url: "/newsletters/popups/add/",
            type: 'GET',
            dataType: 'json',
            
            success: function (response) {
                $("#popup").find(".modal-title").html("<h1>Add newsletter</h1>");
                $("#popup").find(".modal-body").html(response.popup);
                $("#popup") .modal("show");

            },
        });

    })
})(jQuery);