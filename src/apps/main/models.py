from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel

class MainPageConfig(SingletonModel):
    
    class Meta:
        default_permissions = ('change', )
        verbose_name = _('settings')

    def __str__(self):
        return ugettext('Main page')
