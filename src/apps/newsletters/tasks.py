from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from .. .celery import app
from datetime import datetime



@app.task
def _send_newsletter(newsletter, template="newsletters/mails/newsletter.html"):
    for subscriber in newsletter.subscribers.all():
    
        html_message = render_to_string(template, {
            'newsletter': newsletter,
            'subscriber': subscriber,
        })
        plain_message = strip_tags(html_message)
        # django_rq.enqueue(
        send_mail(
            subject="Newsletter from {}".format(settings.DEFAULT_FROM_EMAIL),
            message=plain_message,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[subscriber.email, ],
            fail_silently=False,
            auth_user=None,
            auth_password=None,
            connection=None,
            html_message=html_message
        )
        # )
    newsletter.sended = True
    newsletter.date_sended = datetime.now()
    newsletter.save()
