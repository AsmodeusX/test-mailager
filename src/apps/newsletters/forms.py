from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.validators import EmailValidator
import re
from datetime import datetime
from .models import Newsletter
from subscribers.models import Subscriber


class NewsletterForm(forms.ModelForm):
    postponed = forms.BooleanField(
        label=_("postponed"),
        required=False
    )

    for_all_subscribers = forms.BooleanField(
        label=_("for all subscribers"),
        required=False
    )
    
    subscribers = forms.ModelMultipleChoiceField(
        required=False,
        queryset=Subscriber.objects.all(),
    )
    
    class Meta:
        model = Newsletter
        fields = '__all__'
        exclude = ['sended', 'date_sended', 'count_opened']

