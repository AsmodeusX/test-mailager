(function($) {
    $(document).on("submit", "#fmAddNewsletter", function(e) {
        e.preventDefault();
        var $form = $("#fmAddNewsletter")
        var data = $form.serializeArray();

        $.ajax({
            url: "/newsletters/popups/add/",
            type: 'POST',
            dataType: 'json',
            data: data,
            
            success: function (response) {  
                $("#popup").find(".modal-body").html("Newsletter saved");

            },
            error: function(e) {
                console.log(e)
            }
        });        
    })

    $(document).on("click", "#id_for_all_subscribers", function() {
        $formGroup = $("#id_subscribers").closest(".form-group").eq(0);
        if ($("#id_for_all_subscribers").prop('checked')) {
            $formGroup.hide('slow')
        } else {
            $formGroup.show('slow')
        }
    })
})(jQuery);