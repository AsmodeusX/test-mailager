from .models import Newsletter
from .tasks import _send_newsletter



def send_newsletter(newsletter_id):
    try:
        newsletter = Newsletter.objects.get(id=newsletter_id)
    except (Newsletter.DoesNotExist, Newsletter.MultipleObjectsReturned) as e:
        return {
            'error': e
        }
    
    _send_newsletter.delay(
        newsletter=newsletter,
    )
    return True

