from django.shortcuts import resolve_url
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.utils.html import escape
from django.views.generic import View
from django.utils.translation import ugettext_lazy as _
from .models import Newsletter
from .forms import NewsletterForm
from .utils import send_newsletter
from subscribers.models import Subscriber


class RegisterNewsletter(View):
    def get(self, request, *args, **kwargs):
        newsletter_id = request.GET.get("newsletter_id")

        try:
            newsletter = Newsletter.objects.get(id=int(newsletter_id))
        except:
            return JsonResponse({}, status=400)
        
        newsletter.count_opened += 1
        newsletter.save()


        return JsonResponse({

        }, status=200)


class SendNewsletter(View):
    def get(self, request, *args, **kwargs):
        newsletter_id = request.GET.get("newsletter_id")

        try:
            newsletter = Newsletter.objects.get(id=int(newsletter_id))
        except:
            return JsonResponse({}, status=400)
        send_newsletter(newsletter_id)


        return JsonResponse({

        }, status=200)


class AddNewsletter(View):
    def get(self, request, *args, **kwargs):
        form = NewsletterForm()

        return JsonResponse({
            'popup': render_to_string("newsletters/popups/add.html", {
                "form": form,
            }, request=request)
        }, status=200)
    
    def post(self, request, *args, **kwargs):
        form = NewsletterForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
        else:
            return JsonResponse({
                "error": str(e),
            }, status=400)

        try:
            newsletter = form.save()
            if data['for_all_subscribers']:
                for subscriber in Subscriber.objects.all():
                    newsletter.subscribers.add(subscriber)
                    newsletter.save()
            if not data['postponed']:
                send_newsletter(newsletter.id)

        except Exception as e:
            print(e)
            return JsonResponse({
                "error": str(e),
            }, status=400)
        


        return JsonResponse({
            
        }, status=200)

