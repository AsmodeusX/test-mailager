from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _, ugettext
from django.conf import settings
from subscribers.models import Subscriber


class Newsletter(models.Model):
    title = models.CharField(_("title"), blank=False, max_length=128,)
    text = models.TextField(_("text"), blank=False)
    subscribers = models.ManyToManyField(Subscriber, verbose_name=_("subscribers"), blank=False)
    sended = models.BooleanField(_("Sended"), default=False)
    date_sended = models.DateTimeField(_("date sended"), blank=True, null=True)
    count_opened = models.PositiveSmallIntegerField(_('count opened'), default=0)

    class Meta:
        ordering=("title", )
        verbose_name = _('newsletter')
        verbose_name_plural = _('newsletters')

    def __str__(self):
        return self.title
    
    @property
    def status(self):
        return "Sended {}".format(self.date_sended) if self.sended else _("Not sended")
    
    @property
    def register_link(self):
        return "{}?newsletter_id={}".format(reverse('newsletter:register'), self.id)