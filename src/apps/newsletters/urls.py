from django.conf.urls import url
from .views_ajax import AddNewsletter, SendNewsletter


app_name = 'newsletters'
urlpatterns = [
    url(r'^popups/add/$', AddNewsletter.as_view(), name='popup_add'),
    url(r'^send/$', SendNewsletter.as_view(), name='send'),
]
