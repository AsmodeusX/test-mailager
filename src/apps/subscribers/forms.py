from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.validators import EmailValidator
from .models import Subscriber
import re
from datetime import datetime


class ContactForm(forms.ModelForm):

    class Meta:
        model = Subscriber
        fields = '__all__'
        widgets = {
            'first_name': forms.TextInput(attrs={
                'placeholder': _('John'),
            }),
            'last_name': forms.TextInput(attrs={
                'placeholder': _('Doe'),
            }),
            'email': forms.EmailInput(attrs={
                'placeholder': _('john@gmail.com'),
            }),
            'date_birth': forms.DateField(),
        }
        error_messages = {
            'first_name': {
                'required': _('Please enter first name'),
                'max_length': _('First name should not be longer than %(limit_value)d characters'),
                'min_length': _("The first name is very short"),
            },
            'last_name': {
                'required': _('Please enter last name'),
                'max_length': _('Last name should not be longer than %(limit_value)d characters'),
                'min_length': _("The last name is very short"),
            },
            'email': {
                'required': _('Please enter e-mail'),
                'incorrect': _("E-mail is incorrect"),

            },
            'data_birth': {
                'incorrect': _('Date of birth can\'t be more now'),

            },
        }

    def clean_first_name(self):
        if 'first_name' in self.cleaned_data:
            first_name = self.cleaned_data.get('first_name')

            if not first_name:
                self.add_field_error('first_name', 'required')
            else:

                if len(first_name) < 3:
                    self.add_field_error('first_name', 'min_length')

            return name

    def clean_last_name(self):
        if 'last_name' in self.cleaned_data:
            last_name = self.cleaned_data.get('last_name')

            if not last_name:
                self.add_field_error('last_name', 'required')
            else:

                if len(last_name) < 3:
                    self.add_field_error('last_name', 'min_length')

            return last_name

    def clean_email(self):
        if 'email' in self.cleaned_data:
            email = self.cleaned_data.get('email')

            if not email:
                self.add_field_error('email', 'required')
            else:
                r = re.findall(r'[а-яА-я]', email)

                if len(r) > 0:
                    self.add_field_error('email', 'incorrect')
                else:
                    self.cleaned_data['email'] = email

            return email

    def clean_date_birth(self):
        if 'date_birth' in self.cleaned_data:
            date_birth = self.cleaned_data.get('date_birth')

            if date_birth and date_birth > datetime.now():
                self.add_field_error('date_birth', 'incorrect')
            return date_birth
