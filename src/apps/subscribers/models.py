from django.db import models
from django.utils.translation import ugettext_lazy as _


class Subscriber(models.Model):
    first_name = models.CharField(_("first name"), max_length=64, )
    last_name = models.CharField(_("last name"), max_length=64, )
    date_birth = models.DateField(_("date birth"), null=True, blank=True, )
    email = models.EmailField(_("email"), unique=True)

    class Meta:
        ordering=("first_name", "last_name", )
        verbose_name = _('subscriber')
        verbose_name_plural = _('subscribers')

    @property
    def fullname(self):
        return "{} {}".format(self.first_name, self.last_name)
    
    def __str__(self):
        return self.fullname
