from django.contrib import admin
from .models import Subscriber


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'first_name', 'last_name', 'email', 'date_birth'
            ),
        }),
    )

